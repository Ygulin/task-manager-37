package ru.tsc.gulin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.gulin.tm.enumerated.Status;
import ru.tsc.gulin.tm.exception.field.*;
import ru.tsc.gulin.tm.model.Project;
import ru.tsc.gulin.tm.repository.ProjectRepository;
import ru.tsc.gulin.tm.util.DateUtil;

import java.util.*;

public class ProjectServiceTest {

    /* @NotNull
    private final ProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private static final String USER_ID_1 = UUID.randomUUID().toString();

    @NotNull
    private static final String USER_ID_2 = UUID.randomUUID().toString();

    private static long INITIAL_SIZE;

    @Before
    public void init() {
        projectService.create(USER_ID_1, "project_1", "proj_1");
        projectService.create(USER_ID_2, "project_2", "proj_2");
        projectService.create(USER_ID_2, "project_3", "proj_3");
        INITIAL_SIZE = projectService.getSize();
    }

    @Test
    public void create() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.create("", "project"));
        Assert.assertThrows(NameEmptyException.class, () -> projectService.create(USER_ID_1, ""));
        @NotNull final Project project = projectService.create(USER_ID_1, "project");
        Assert.assertEquals(INITIAL_SIZE + 1, projectService.getSize());
        Assert.assertNotNull(project.getName());
    }

    @Test
    public void createWithDescription() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.create("", "project"));
        Assert.assertThrows(NameEmptyException.class, () -> projectService.create(USER_ID_1, ""));
        Assert.assertThrows(DescriptionEmptyException.class, () -> projectService.create(USER_ID_1, "project", ""));
        @NotNull final Project project = projectService.create(USER_ID_1, "project", "description");
        Assert.assertEquals(INITIAL_SIZE + 1, projectService.getSize());
        Assert.assertNotNull(project.getDescription());
    }

    @Test
    public void createWithDescriptionAndDate() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.create("", "project"));
        @NotNull final Project project = projectService.create(
                USER_ID_1,
                "name",
                "description",
                DateUtil.toDate("01.01.2021"),
                DateUtil.toDate("01.10.2021")
        );
        Assert.assertEquals(INITIAL_SIZE + 1, projectService.getSize());
        Assert.assertNotNull(project.getDateBegin());
        Assert.assertNotNull(project.getDateEnd());
    }

    @Test
    public void findAll() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findAll(""));
        @NotNull final List<Project> projects = projectService.findAll();
        Assert.assertEquals(INITIAL_SIZE, projects.size());
        @NotNull final List<Project> userProjects = projectService.findAll(USER_ID_2);
        Assert.assertEquals(2, userProjects.size());
        @NotNull final List<Project> newUserProjects = projectService.findAll(UUID.randomUUID().toString());
        Assert.assertEquals(0, newUserProjects.size());
    }

    @Test
    public void add() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.add("", new Project()));
        @Nullable Project project = null;
        Assert.assertNull(projectService.add(USER_ID_1, project));
        project = new Project();
        projectService.add(USER_ID_1, project);
        Assert.assertEquals(INITIAL_SIZE + 1, projectService.getSize());
    }

    @Test
    public void findOneByIndex() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findOneByIndex("", 1));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.findOneByIndex(USER_ID_1, -4));
        @NotNull final String projectName = "proj find by index";
        projectService.create(USER_ID_1, projectName);
        @NotNull final Project project = projectService.findOneByIndex(USER_ID_1, 1);
        Assert.assertEquals(projectName, project.getName());
    }

    @Test
    public void findOneById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findOneById("", "1"));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.findOneById(USER_ID_1, ""));
        @NotNull final String projectName = "proj find by id";
        @NotNull final Project project = projectService.create(USER_ID_1, projectName);
        @NotNull final String id = project.getId();
        Assert.assertNotNull(projectService.findOneById(USER_ID_1, id));
        Assert.assertEquals(projectName, projectService.findOneById(USER_ID_1, id).getName());
    }

    @Test
    public void remove() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.remove("", new Project()));
        Assert.assertNull(projectService.remove(USER_ID_1, null));
        @NotNull final Project project = projectService.create(USER_ID_1, "project remove");
        projectService.remove(USER_ID_1, project);
        Assert.assertEquals(INITIAL_SIZE, projectService.getSize());
        Assert.assertNull(projectService.findOneById(project.getId()));
    }

    @Test
    public void removeByIndex() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.removeByIndex("", 1));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.removeByIndex(USER_ID_1, -4));
        @NotNull final Integer index = 1;
        projectService.create(USER_ID_1, "project remove by index");
        projectService.removeByIndex(USER_ID_1, 1);
        Assert.assertEquals(INITIAL_SIZE, projectService.getSize());
    }

    @Test
    public void removeById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.removeById("", "1"));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.removeById(USER_ID_1, ""));
        @NotNull final Project project = projectService.create(USER_ID_1, "project remove");
        @NotNull final String id = project.getId();
        projectService.removeById(USER_ID_1, id);
        Assert.assertEquals(INITIAL_SIZE, projectService.getSize());
        Assert.assertNull(projectService.findOneById(id));
    }

    @Test
    public void clear() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.clear(""));
        projectService.clear(USER_ID_1);
        Assert.assertEquals(0, projectService.getSize(USER_ID_1));
        Assert.assertEquals(INITIAL_SIZE - 1, projectService.getSize());
        projectService.clear();
        Assert.assertEquals(0, projectService.getSize());
    }

    @Test
    public void existsById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.existsById("", "1"));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.existsById(USER_ID_1, ""));
        @NotNull final Project project = projectService.create(USER_ID_1, "project exists");
        Assert.assertTrue(projectService.existsById(USER_ID_1, project.getId()));
    }

    @Test
    public void getSize() {
        @NotNull final ProjectRepository repository = new ProjectRepository();
        @NotNull final ProjectService service = new ProjectService(repository);
        Assert.assertThrows(UserIdEmptyException.class, () -> service.getSize(""));
        Assert.assertEquals(0, service.getSize());
        service.create(USER_ID_1, "project");
        Assert.assertEquals(1, service.getSize());
        Assert.assertEquals(0, service.getSize(USER_ID_2));
    }

    @Test
    public void updateByIndex() {
        Assert.assertThrows(UserIdEmptyException.class,
                () -> projectService.updateByIndex("", 0, "new", "new descr")
        );
        Assert.assertThrows(IndexIncorrectException.class,
                () -> projectService.updateByIndex(USER_ID_1, -1, "new", "new descr")
        );
        Assert.assertThrows(NameEmptyException.class,
                () -> projectService.updateByIndex(USER_ID_1, 0, "", "new descr")
        );
        Assert.assertThrows(DescriptionEmptyException.class,
                () -> projectService.updateByIndex(USER_ID_1, 0, "new", "")
        );
        @NotNull final Integer index = 1;
        @NotNull final String name = "new name";
        @NotNull final String description = "new description";
        @NotNull final Project project = projectService.updateByIndex(USER_ID_1, 0, name, description);
        Assert.assertEquals(name, project.getName());
        Assert.assertEquals(description, project.getDescription());
    }

    @Test
    public void updateById() {
        Assert.assertThrows(UserIdEmptyException.class,
                () -> projectService.updateById("", "123", "new", "new descr")
        );
        Assert.assertThrows(IdEmptyException.class,
                () -> projectService.updateById(USER_ID_1, "", "new", "new descr")
        );
        Assert.assertThrows(NameEmptyException.class,
                () -> projectService.updateById(USER_ID_1, "123", "", "new descr")
        );
        Assert.assertThrows(DescriptionEmptyException.class,
                () -> projectService.updateById(USER_ID_1, "123", "new", "")
        );
        @NotNull final String name = "new name";
        @NotNull final String description = "new description";
        @NotNull final String id = projectService.create(USER_ID_1, "old name").getId();
        @NotNull final Project project = projectService.updateById(USER_ID_1, id, name, description);
        Assert.assertEquals(name, project.getName());
        Assert.assertEquals(description, project.getDescription());
    }

    @Test
    public void changeProjectStatusByIndex() {
        Assert.assertThrows(UserIdEmptyException.class,
                () -> projectService.changeProjectStatusByIndex("", 0, Status.IN_PROGRESS)
        );
        Assert.assertThrows(IndexIncorrectException.class,
                () -> projectService.changeProjectStatusByIndex(USER_ID_1, -1, Status.NOT_STARTED)
        );
        Assert.assertThrows(StatusEmptyException.class,
                () -> projectService.changeProjectStatusByIndex(USER_ID_1, 0, Status.toStatus(""))
        );
        @NotNull final Status status = Status.IN_PROGRESS;
        @NotNull final Project project = projectService.changeProjectStatusByIndex(USER_ID_1, 0, status);
        Assert.assertEquals(status, project.getStatus());
    }

    @Test
    public void changeProjectStatusById() {
        Assert.assertThrows(UserIdEmptyException.class,
                () -> projectService.changeProjectStatusById("", "123", Status.IN_PROGRESS)
        );
        Assert.assertThrows(IdEmptyException.class,
                () -> projectService.changeProjectStatusById(USER_ID_1, "", Status.NOT_STARTED)
        );
        Assert.assertThrows(StatusEmptyException.class,
                () -> projectService.changeProjectStatusById(USER_ID_1, "123", Status.toStatus(""))
        );
        @NotNull final Status status = Status.IN_PROGRESS;
        @NotNull final String id = projectService.create(USER_ID_1, "project change status").getId();
        @NotNull final Project project = projectService.changeProjectStatusById(USER_ID_1, id, status);
        Assert.assertEquals(status, project.getStatus());
    } */

}
