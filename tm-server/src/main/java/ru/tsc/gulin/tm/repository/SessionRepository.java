package ru.tsc.gulin.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.gulin.tm.api.repository.ISessionRepository;
import ru.tsc.gulin.tm.enumerated.Role;
import ru.tsc.gulin.tm.model.Session;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    @NotNull
    private final String TABLE_NAME = "TM_TASK";

    public SessionRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Override
    @NotNull
    protected String getTableName() {
        return TABLE_NAME;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Session fetch(@NotNull final ResultSet row) {
        @NotNull final Session session = new Session();
        session.setId(row.getString("id"));
        session.setDate(row.getTimestamp("date"));
        session.setUserId(row.getString("user_id"));
        session.setRole(Role.valueOf(row.getString("role")));
        return session;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Session add(@NotNull final Session session) {
        @NotNull final String sql = "INSERT INTO " + getTableName()+
                " (id, date, user_id, role) VALUES (?, ?, ?, ?)";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, session.getId());
            statement.setTimestamp(2, new Timestamp(session.getDate().getTime()));
            statement.setString(3, session.getUserId());
            statement.setString(4, session.getRole().toString());
            statement.executeUpdate();
        }
        return session;
    }

    @NotNull
    @Override
    public Session add(@NotNull final String userId, @NotNull final Session session) {
        session.setUserId(userId);
        return add(session);
    }

    @Override
    @SneakyThrows
    public Session update(@NotNull final Session session) {
        @NotNull final String sql =
                "UPDATE " + getTableName()
                        + " SET date = ?, user_id = ?, role = ? WHERE id = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setTimestamp(1, new Timestamp(session.getDate().getTime()));
            statement.setString(2, session.getUserId());
            statement.setString(3, session.getRole().toString());
            statement.setString(4, session.getId());
            statement.executeUpdate();
        }
        return session;
    }

}
