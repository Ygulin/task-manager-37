package ru.tsc.gulin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.gulin.tm.model.Session;

import java.sql.ResultSet;

public interface ISessionRepository extends IUserOwnedRepository<Session> {

    @NotNull
    Session fetch(@NotNull ResultSet row);

    @NotNull
    Session add(@NotNull Session session);

    @NotNull
    Session add(@NotNull String userId, @NotNull Session session);

    Session update(@NotNull Session session);

}
