package ru.tsc.gulin.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.api.repository.IUserRepository;
import ru.tsc.gulin.tm.enumerated.Role;
import ru.tsc.gulin.tm.enumerated.Status;
import ru.tsc.gulin.tm.model.Task;
import ru.tsc.gulin.tm.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.UUID;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    private final String TABLE_NAME = "TM_USER";

    public UserRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User fetch(@NotNull final ResultSet row) {
        @NotNull final User user = new User();
        user.setId(row.getString("id"));
        user.setLogin(row.getString("login"));
        user.setPasswordHash(row.getString("password"));
        user.setEmail(row.getString("email"));
        user.setRole(Role.valueOf(row.getString("role")));
        user.setLastName(row.getString("last_name"));
        user.setFirstName(row.getString("first_name"));
        user.setMiddleName(row.getString("middle_name"));
        user.setLocked(row.getBoolean("lock"));
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User add(@NotNull final User user) {
        @NotNull final String sql = "INSERT INTO " + getTableName()+
                " (id, login, password, email, role, last_name, first_name, middle_name, lock) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, UUID.randomUUID().toString());
            statement.setString(2, user.getLogin());
            statement.setString(3, user.getPasswordHash());
            statement.setString(4, user.getEmail());
            statement.setString(5, user.getRole().toString());
            statement.setString(6, user.getLastName());
            statement.setString(7, user.getFirstName());
            statement.setString(8, user.getMiddleName());
            statement.setBoolean(9, user.getLocked());
            statement.executeUpdate();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User update(@NotNull final User user) {
        @NotNull final String sql =
                "UPDATE " + getTableName()
                        + " SET login = ?, password = ?, email = ?, role = ?, last_name = ?, first_name = ?, middle_name = ?, lock = ? WHERE id = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getPasswordHash());
            statement.setString(3, user.getEmail());
            statement.setString(4, user.getRole().toString());
            statement.setString(5, user.getLastName());
            statement.setString(6, user.getFirstName());
            statement.setString(7, user.getMiddleName());
            statement.setString(8, user.getLocked().toString());
            statement.setString(9, user.getId());
            statement.executeUpdate();
        }
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findOneByLogin(@NotNull final String login) {
        @NotNull final String sql = "SELECT id, login, password, email, role, last_name, first_name ,middle_name, lock FROM "
                + getTableName()+ " WHERE login = ? LIMIT 1 ";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, login);
        @NotNull final ResultSet rowSet = statement.executeQuery();
        if (!rowSet.next()) return null;
        return fetch(rowSet);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findOneByEmail(@NotNull final String email) {
        @NotNull final String sql = "SELECT id, login, password, email, role, last_name, first_name ,middle_name, lock FROM "
                + getTableName()+ " WHERE email = ? LIMIT 1 ";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, email);
        @NotNull final ResultSet rowSet = statement.executeQuery();
        if (!rowSet.next()) return null;
        return fetch(rowSet);
    }

    @Nullable
    @Override
    public User removeByLogin(@NotNull final String login) {
        @Nullable final User user = findOneByLogin(login);
        if (user == null) return null;
        return remove(user);
    }

}
