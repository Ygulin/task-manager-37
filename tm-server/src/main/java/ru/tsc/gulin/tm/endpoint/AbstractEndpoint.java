package ru.tsc.gulin.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.api.service.IServiceLocator;
import ru.tsc.gulin.tm.dto.request.AbstractUserRequest;
import ru.tsc.gulin.tm.enumerated.Role;
import ru.tsc.gulin.tm.exception.system.AccessDeniedException;
import ru.tsc.gulin.tm.model.Session;

import java.util.Optional;

@NoArgsConstructor
public abstract class AbstractEndpoint {

    protected Session check (@Nullable final AbstractUserRequest request, @Nullable final Role role) {
        Optional.ofNullable(request).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(role).orElseThrow(AccessDeniedException::new);
        @Nullable final String token = request.getToken();
        @NotNull Session session = serviceLocator.getAuthService().validateToken(token);
        Optional.ofNullable(session.getRole()).orElseThrow(AccessDeniedException::new);
        if (!session.getRole().equals(role)) throw new AccessDeniedException();
        return session;
    }

    protected Session check (@Nullable final AbstractUserRequest request) {
        Optional.ofNullable(request).orElseThrow(AccessDeniedException::new);
        @Nullable final String token = request.getToken();
        Optional.ofNullable(token).filter(item -> !item.isEmpty()).orElseThrow(AccessDeniedException::new);
        return serviceLocator.getAuthService().validateToken(token);
    }

    @Getter
    @Nullable
    private IServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

}
