package ru.tsc.gulin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.enumerated.Sort;
import ru.tsc.gulin.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    @NotNull
    List<M> findAll(@NotNull String userId);

    @NotNull
    List<M> findAll(@NotNull String userId, @NotNull Comparator<M> comparator);

    @NotNull
    List<M> findAll(@NotNull String userId, @NotNull Sort sort);

    @Nullable
    M add(@NotNull String userId, @NotNull M model);

    @Nullable
    M findOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    M remove(@NotNull String userId, @NotNull M model);

    @Nullable
    M removeById(@NotNull String userId, @NotNull String id);

    void clear(@NotNull String userId);

    boolean existsById(@NotNull String userId, @NotNull String id);

    long getSize(@NotNull String userId);

}
