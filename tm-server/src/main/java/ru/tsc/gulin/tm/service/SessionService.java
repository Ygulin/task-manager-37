package ru.tsc.gulin.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.gulin.tm.api.repository.ISessionRepository;
import ru.tsc.gulin.tm.api.service.IConnectionService;
import ru.tsc.gulin.tm.api.service.ISessionService;
import ru.tsc.gulin.tm.model.Session;
import ru.tsc.gulin.tm.repository.SessionRepository;

import java.sql.Connection;

public final class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    public ISessionRepository getRepository(@NotNull Connection connection) {
        return new SessionRepository(connection);
    }

}
