package ru.tsc.gulin.tm.api.repository;

import ru.tsc.gulin.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

}
