package ru.tsc.gulin.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.api.repository.ITaskRepository;
import ru.tsc.gulin.tm.enumerated.Status;
import ru.tsc.gulin.tm.model.Project;
import ru.tsc.gulin.tm.model.Task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @NotNull
    private final String TABLE_NAME = "TM_TASK";

    public TaskRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task fetch(@NotNull final ResultSet row) {
        @NotNull final Task task = new Task();
        task.setId(row.getString("id"));
        task.setName(row.getString("name"));
        task.setDescription(row.getString("description"));
        task.setStatus(Status.toStatus(row.getString("status")));
        task.setUserId(row.getString("user_id"));
        task.setCreated(row.getTimestamp("created_dt"));
        task.setDateBegin(row.getTimestamp("start_dt"));
        task.setDateEnd(row.getTimestamp("end_dt"));
        return task;
    }

    @Nullable
    @Override
    public Task add(@NotNull final String userId, @NotNull final Task task) {
        task.setUserId(userId);
        return add(task);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task add(@NotNull final Task task) {
        @NotNull final String sql = "INSERT INTO " + getTableName()+
                " (id, created_dt, name, description, status, user_id, start_dt, end_dt) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, UUID.randomUUID().toString());
            statement.setTimestamp(2, new Timestamp(task.getCreated().getTime()));
            statement.setString(3, task.getName());
            statement.setString(4, task.getDescription());
            statement.setString(5, task.getStatus().toString());
            statement.setString(6, task.getUserId());
            if (task.getDateBegin() != null) {
                statement.setTimestamp(7, new Timestamp(task.getDateBegin().getTime()));
            } else statement.setTimestamp(7, new Timestamp(System.currentTimeMillis()));
            if (task.getDateEnd() != null) {
                statement.setTimestamp(8, new Timestamp(task.getDateEnd().getTime()));
            } else statement.setTimestamp(8, new Timestamp(System.currentTimeMillis()));
            statement.executeUpdate();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task update(@NotNull final Task task) {
        @NotNull final String sql =
                "UPDATE " + getTableName()+ " SET name = ?, description = ?, status = ? WHERE id = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, task.getName());
            statement.setString(2, task.getDescription());
            statement.setString(3, task.getStatus().toString());
            statement.setString(4, task.getId());
            statement.executeUpdate();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId) {
        @NotNull final List<Task> result = new ArrayList<>();
        @NotNull final String sql =
                "SELECT id, created_dt, name, description, status, user_id, project_id, start_dt, end_dt FROM "
                        + getTableName()+ " WHERE user_id = ? AND project_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, userId);
        statement.setString(2, projectId);
        @NotNull final ResultSet rowSet = statement.executeQuery();
        while (rowSet.next()) result.add(fetch(rowSet));
        return result;
    }

}
