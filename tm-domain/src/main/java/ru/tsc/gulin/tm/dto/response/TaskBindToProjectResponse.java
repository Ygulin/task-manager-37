package ru.tsc.gulin.tm.dto.response;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class TaskBindToProjectResponse extends AbstractResponse {
}
