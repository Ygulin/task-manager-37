package ru.tsc.gulin.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.api.model.IWBS;
import ru.tsc.gulin.tm.enumerated.Status;

import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
public final class Task extends AbstractUserOwnedModel implements IWBS {

    private static final long serialVersionUID = 1;

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private Status status = Status.NOT_STARTED;

    @Nullable
    private String projectId;

    @NotNull
    private Date created = new Date();

    @Nullable
    private Date dateBegin;

    @Nullable
    private Date dateEnd;

    public Task(
            @NotNull final String name,
            @NotNull final Status status,
            @NotNull final String description,
            @NotNull final Date dateBegin
    ) {
        this.name = name;
        this.status = status;
        this.description = description;
        this.dateBegin = dateBegin;
    }

    @NotNull
    @Override
    public String toString() {
        return name + " : " +
                getStatus().getDisplayName() + " : " + description;
    }

}
