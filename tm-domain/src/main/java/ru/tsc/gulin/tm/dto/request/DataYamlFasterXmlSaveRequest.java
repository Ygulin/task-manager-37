package ru.tsc.gulin.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class DataYamlFasterXmlSaveRequest extends AbstractUserRequest {

    public DataYamlFasterXmlSaveRequest(@Nullable final String token) {
        super(token);
    }

}
