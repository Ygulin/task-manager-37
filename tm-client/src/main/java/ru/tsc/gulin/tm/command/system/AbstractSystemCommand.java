package ru.tsc.gulin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.api.service.ICommandService;
import ru.tsc.gulin.tm.command.AbstractCommand;
import ru.tsc.gulin.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    @NotNull
    public ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
