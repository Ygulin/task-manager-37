package ru.tsc.gulin.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.dto.request.DataYamlFasterXmlSaveRequest;
import ru.tsc.gulin.tm.enumerated.Role;

public final class DataYamlFasterXmlSaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-yaml-fasterxml";

    @NotNull
    public static final String DESCRIPTION = "Save data in yaml file";

    @Override
    public @NotNull String getName() {
        return NAME;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @Nullable Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[SAVE DATA IN YAML FILE]");
        getDomainEndpoint().saveDataYamlFasterXml(new DataYamlFasterXmlSaveRequest(getToken()));
    }

}
